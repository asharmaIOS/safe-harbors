//
//  ValidationAndJsonVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 1/31/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "ValidationAndJsonVC.h"
#import "SVProgressHUD.h"


@interface ValidationAndJsonVC ()

@end

@implementation ValidationAndJsonVC

@synthesize serverCheckStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}




+(NSDictionary *)getParsingData:(NSDictionary *)ParsingDict GetUrl:(NSString *)urlStr GetTimeinterval:(NSTimeInterval)interval
{
    __block NSDictionary *jsonDic = nil;
    NSLog(@"%@",ParsingDict);
    
    
    [self makeRequestDictionary:ParsingDict withBaseURL:urlStr completion:^(NSData *data, NSError *error) {
        
        if (error == nil) {
            NSError *err;
            jsonDic = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &err];
            
            NSLog(@"print   ===%@",jsonDic);
            
        } else {
            jsonDic = nil;
            
            NSLog(@"print   ===%@",jsonDic);
        }
    }];
    
    
    //[SVProgressHUD dismiss];
    
    return jsonDic;
}

+ (void)makeRequestDictionary:(NSDictionary*)param withBaseURL:(NSString*)strURL completion:(void (^)(NSData *data, NSError *error))completion {
    NSURL *url;
    
    if ([strURL isEqualToString:@"api/v1/alert/get"]) {
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[AppDelegate sharedAppDelegate].BASEURLNOTIFDETAILS,strURL]];

        
    }
    
    else {
        
       url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[AppDelegate sharedAppDelegate].BASEAPIPNR,strURL]];
    }
        
   
    NSLog(@"url%@=======",url);
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30;
    config.timeoutIntervalForResource = 30;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSData *data = [NSJSONSerialization dataWithJSONObject:param
                                                   options:kNilOptions error:nil];
    request.HTTPBody = data;
    
    dispatch_semaphore_t semaphore;
    semaphore = dispatch_semaphore_create(0);
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse * response, NSError *error) {
        
        if (!error) {
            completion(data, nil);
            
            
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, response);
            completion(nil, error);
//            NSData *errorData = [error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"];
//            NSDictionary *responseErrorObject = [NSJSONSerialization JSONObjectWithData:errorData options:NSJSONReadingAllowFragments error:nil];
//            NSLog(@"%@",responseErrorObject);
        }
        
        dispatch_semaphore_signal(semaphore);
    }];
    
    [postDataTask resume];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    
}














+(NSDictionary *)GetParsingNewData:(NSDictionary *)ParsingDict GetUrl:(NSString *)urlStr GetTimeinterval:(NSTimeInterval)interval
{
    __block NSDictionary *jsonDic = nil;
    NSLog(@"%@",ParsingDict);
    
    
    [self makeRequestDic:ParsingDict withBaseURL:urlStr completion:^(NSData *data, NSError *error) {
        
        if (error == nil) {
            NSError *err;
            jsonDic = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &err];
            
            NSLog(@"print   ===%@",jsonDic);
           
        } else {
            jsonDic = nil;
            
            NSLog(@"print   ===%@",jsonDic);
        }
    }];
    

    [SVProgressHUD dismiss];
    
    return jsonDic;
}






+ (void)makeRequestDic:(NSDictionary*)param withBaseURL:(NSString*)strURL completion:(void (^)(NSData *data, NSError *error))completion {
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[AppDelegate sharedAppDelegate].BASEAPI,strURL]];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30;
    config.timeoutIntervalForResource = 30;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSData *data = [NSJSONSerialization dataWithJSONObject:param
                                                   options:kNilOptions error:nil];
    request.HTTPBody = data;
    
    dispatch_semaphore_t semaphore;
    semaphore = dispatch_semaphore_create(0);
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse * response, NSError *error) {
        
        if (!error) {
            completion(data, nil);
        
           
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, response);
            completion(nil, error);
//            NSData *errorData = [error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"];
//            NSDictionary *responseErrorObject = [NSJSONSerialization JSONObjectWithData:errorData options:NSJSONReadingAllowFragments error:nil];
//            NSLog(@"%@",responseErrorObject);
        }
        
 dispatch_semaphore_signal(semaphore);
    }];
    
    [postDataTask resume];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    
}

+(void)displayAlert:(NSString*)erroemessage HeaderMsg:(NSString*)HeaderMsg {
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:HeaderMsg message:erroemessage preferredStyle:UIAlertControllerStyleAlert];
//    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:erroemessage];
//    [hogan addAttribute:NSFontAttributeName
//                  value:[UIFont fontWithName:@"ClanOffc-News" size:13]
//                  range:NSMakeRange(0,erroemessage.length )];
//    [alertVC setValue:hogan forKey:@"attributedTitle"];
    UIAlertAction *button = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action){
                                                       //add code to make something happen once tapped
                                                   }];
    [alertVC addAction:button];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    if([rootViewController isKindOfClass:[UITabBarController class]])
    {
        rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
    }
    
    [rootViewController presentViewController:alertVC animated:YES completion:nil];
    
}



@end
