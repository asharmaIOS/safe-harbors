//
//  CityInfoBySearchVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 1/2/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "CityInfoBySearchVC.h"
#import "SearchDestinationVC.h"
#import "TravelConstantClass.h"
#import "SideSeenCell.h"
#import "HorizantalCell.h"
#import "XMLReader.h"
#import "SVProgressHUD.h"
#import "UIFont+Extras.h"
#import "DemoMessagesViewController.h"


#import "ReadMoreDescriptionOfdestinationViewController.h"

@interface CityInfoBySearchVC ()
{
    UIScrollView *scrollView;
    NSMutableArray *segmentArrayOfButtons;
    NSArray *completeDataArray;
    NSMutableArray *dataForReadmore;
    UIScrollView *scrollViewForSepcificData;
    NSString *strCityNameWithState;
}

@property (weak, nonatomic) IBOutlet UILabel *citynamelbl;

@end

@implementation CityInfoBySearchVC

- (void)viewDidLoad {
    
    [self.navigationController.navigationBar setHidden: NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.0 green:075/255.0 blue:141/255.0 alpha:1.0];
    
    
   
    segmentArrayOfButtons = [[NSMutableArray alloc] init];
    dataForReadmore = [[NSMutableArray alloc] init];
     [self setupLeftMenuButton];
    [super viewDidLoad];
}

-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.leftBarButtonItems = @[searchItem];
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

    


- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setHidden: NO];

    [self loadDataAccordingCity];
}


-(void)backButtonTapp:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)loadDataAccordingCity
{
    _citynamelbl.text = _strCity;
    _strCity = [_strCity stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeBlack];
    NSString *stringUrl = [NSString stringWithFormat:@"http://api.arrivalguides.com/api/xml/Travelguide?auth=057bbab0770837e2364596fb21d6cf0e35ac9e44&lang=en&limit=2&skip=2&v=13&iso=%@",_strCity];
    NSURL *url = [NSURL URLWithString:stringUrl];
    
    NSData *xmlData = [[NSData alloc] initWithContentsOfURL:url];
    NSError *parseError = nil;
    NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:xmlData error:&parseError];
    NSLog(@"%@", xmlDictionary);
    
    // From Complete Array finding section Array ************
    
    NSArray *sectionArray=[[[xmlDictionary objectForKey:@"destination"]objectForKey:@"sections"]objectForKey:@"section"];
    NSMutableArray *dataArray = [self arrayOfSegment:sectionArray];
    completeDataArray = sectionArray;
    [self loadDataAccordingButtonSelect:sectionArray indexOfbuttonSelect:0];
    
    NSArray *segmentArray = [NSArray arrayWithArray:dataArray];
    [self drawSegmentController:segmentArray];
    NSString *cityimage=[[[xmlDictionary objectForKey:@"destination"]objectForKey:@"icon"]objectForKey:@"url"];
    
    strCityNameWithState = [[xmlDictionary objectForKey:@"destination"]objectForKey:@"name"];
    
    [self loadMainImage:cityimage];
   
 
     [SVProgressHUD dismiss];
    
}

- (void)loadMainImage:(NSString *)mainImageStr {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
//CGFloat screenHeight = screenRect.size.height;
    
    UIImageView *imageview = [[UIImageView alloc]
                              initWithFrame:CGRectMake(0, 44, screenWidth, 250)];
   // UIImage *imageForMain = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:imageview.center];
    [imageview addSubview:indicator];
    
    
    [self downloadImageWithURL:[NSURL URLWithString: mainImageStr] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
          
            imageview.image = image;
            [indicator removeFromSuperview];
            [self loadTextOverMainImageView:imageview];

        }
    }];
    
    
   // imageview.image = mainImage;
    [imageview setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imageview];
    
}

-(void)loadTextOverMainImageView:(UIImageView *)imageView{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 125, screenWidth-40, 22)];
    namelabel.textColor = [UIColor whiteColor];
    namelabel.font = [UIFont systemFontOfSize:18];
    namelabel.textAlignment = NSTextAlignmentCenter;
    namelabel.text = @"WELCOME TO";
    [imageView addSubview:namelabel];
    
    
    UILabel *cityNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 145, screenWidth-40, 30)];
    cityNameLbl.textColor = [UIColor whiteColor];
    cityNameLbl.font = [UIFont boldSystemFontOfSize:26];
    cityNameLbl.textAlignment = NSTextAlignmentCenter;
    cityNameLbl.text = strCityNameWithState;
    [imageView addSubview:cityNameLbl];
    
    
}





-(NSMutableArray *)arrayOfSegment:(NSArray *)dataArray {
    
    NSMutableArray *segmentArray = [[NSMutableArray alloc]init ];
    
    for (int i= 0; i<dataArray.count; i++) {
        [segmentArray addObject:[[dataArray objectAtIndex:i ]valueForKey:@"name"]];
    }
    
    return segmentArray;
}

-(void)drawSegmentController:(NSArray *)dataArray {

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    for(UIView *subview in [scrollView subviews]) {
        [subview removeFromSuperview];
    }
    [segmentArrayOfButtons removeAllObjects];
    
    scrollView = [[UIScrollView alloc] init];
    //scrollView.backgroundColor = [UIColor grayColor ];
    scrollView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
    scrollView.frame = CGRectMake(0, 295, screenWidth , 45);
    
    CGFloat finalWidth = 10;
    
    for (int i = 0; i <dataArray.count ; i++) {
        
        CGSize stringsize = [ [dataArray objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:17]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = i;
        button.titleLabel.font = [UIFont systemFontOfSize:17];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        if (i==0) {
            
            [button setTitleColor:[UIColor colorWithRed:0/255.0 green:075/255.0 blue:141/255.0 alpha:1.0] forState:UIControlStateNormal];
             
        }

        [button addTarget:self
                   action:@selector(buttonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
       
        [button setFrame:CGRectMake(finalWidth,05,stringsize.width, 35)];
         [scrollView addSubview:button];
        [segmentArrayOfButtons addObject:button ];
        finalWidth = finalWidth + button.frame.size.width+ 35;
    }
    

    scrollView.showsHorizontalScrollIndicator = NO;

    scrollView.contentSize = CGSizeMake(finalWidth, 40);
    [self.view addSubview:scrollView];
    
}

-(void) readMoreBtnClick:(UIButton*)sender {
    //dataForReadmore
    if (dataForReadmore.count!=0) {
        NSDictionary *dic = [dataForReadmore objectAtIndex:sender.tag];
        
       // ReadMoreDescriptionOfdestinationViewController *vc  = [[ReadMoreDescriptionOfdestinationViewController alloc] initWithNibName:@"ReadMoreDescriptionOfdestinationViewController" bundle:nil];
        
        ReadMoreDescriptionOfdestinationViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadMoreDescriptionOfdestinationViewController"];

        vc.dataDic =dic;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}


-(void) buttonClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %d", sender.tag);
    
    
    for (int i = 0; i<segmentArrayOfButtons.count; i++) {
        
        if (i == sender.tag) {
             [[segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor colorWithRed:0/255.0 green:075/255.0 blue:141/255.0 alpha:1.0] forState:UIControlStateNormal];
        }
        else {
           [ [segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        

    }
    
  //  UIView *view; //View to center
   // UIScrollView *scrollView; //scroll view
    
    CGPoint point = CGPointMake(sender.frame.origin.x + sender.frame.size.width / 2,
                                sender.frame.origin.y + sender.frame.size.height / 2);
    
    CGRect rectToZoom = CGRectMake(point.x * scrollView.zoomScale - roundf(scrollView.frame.size.width /2.),
                                   point.y * scrollView.zoomScale - roundf(scrollView.frame.size.height /2.),
                                   scrollView.frame.size.width,
                                   scrollView.frame.size.height);
    
    [scrollView scrollRectToVisible:rectToZoom animated:YES];
    
 
      [self loadDataAccordingButtonSelect:completeDataArray indexOfbuttonSelect:sender.tag];
}

-(void) loadDataAccordingButtonSelect:(NSArray *)dataArray indexOfbuttonSelect:(int)index {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat finalHeight = 0;
    
    [dataForReadmore removeAllObjects];
    
    if (!scrollViewForSepcificData) {
        scrollViewForSepcificData = [[UIScrollView alloc] init];
        scrollViewForSepcificData.frame = CGRectMake(0, 336, screenWidth , 500);
        [self.view  addSubview: scrollViewForSepcificData];
    }
    

    for(UIView *subview in [scrollViewForSepcificData subviews]) {
        [subview removeFromSuperview];
    }
    
    scrollViewForSepcificData.contentOffset = CGPointMake(0,0);
    
    if ([[dataArray objectAtIndex:index] valueForKey:@"description"] != nil) {
        
        UITextView *textView = [[UITextView alloc]init ];
        textView.userInteractionEnabled = NO;
        textView.frame = CGRectMake(7, 5, screenWidth-14, 300);
       // textView.font = [UIFont fontWithName:@"Lato" size:16];
        textView.backgroundColor = [UIColor clearColor];
        textView.text = [[dataArray objectAtIndex:index] valueForKey:@"description"];
    
    
    NSString *textViewText = textView.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7;
    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:17.0f] };
    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
    
    textView.attributedText = attributedString;
    textView.frame = CGRectMake(07, 05, screenWidth-14, textView.contentSize.height);
    [scrollViewForSepcificData addSubview:textView];
    
        finalHeight = textView.contentSize.height;
        
    }
    
     if ([[dataArray objectAtIndex:index] valueForKey:@"pointsofinterest"] != nil) {
         
         UIView *imageBackgroundView = [[UIView alloc] init];
         imageBackgroundView.frame = CGRectMake(0, finalHeight+15, screenWidth , 340);
         imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
         [scrollViewForSepcificData addSubview: imageBackgroundView];
         
        UIScrollView *scrollViewForImage = [[UIScrollView alloc] init];
         scrollViewForImage.frame = CGRectMake(0, 0, screenWidth , imageBackgroundView.frame.size.height);
         [imageBackgroundView addSubview:scrollViewForImage];
         scrollViewForImage.showsHorizontalScrollIndicator = NO;

         
         
         finalHeight = finalHeight + 340;
         NSArray *entryArray = [[[dataArray objectAtIndex:index] valueForKey:@"pointsofinterest"]valueForKey:@"entry"];
         dataForReadmore = [entryArray mutableCopy];
         
         for (int i = 0; i< entryArray.count; i++) {

          
             UIView *imageConatainer = [[UIView alloc] init];
             imageConatainer.frame = CGRectMake(20+ 320*i, 20, 300, 300);
             imageConatainer.backgroundColor = [UIColor whiteColor];
             imageConatainer.layer.cornerRadius = 7;
             imageConatainer.layer.masksToBounds = true;

             
             
             [scrollViewForImage addSubview: imageConatainer];
             
             
             
         UIImageView *imageviewFor = [[UIImageView alloc]init];
             imageviewFor.backgroundColor = [UIColor grayColor];
         imageviewFor.frame = CGRectMake(0, 0, 300, 170);
             
             if ([[[entryArray objectAtIndex:i] valueForKey:@"images"] isKindOfClass:[NSDictionary class]]) {
             
         NSDictionary *dicImage = [[[entryArray objectAtIndex:i] valueForKey:@"images"] valueForKey:@"image"];
         //NSString * imageUrl = [dicImage valueForKey:@"url"];
                 NSString * imageUrl ;
                 if ([[dicImage valueForKey:@"url"] isKindOfClass:[NSArray class]]) {
                     imageUrl = [[dicImage valueForKey:@"url"] objectAtIndex:0];
                 }
                 else {
                     imageUrl =  [dicImage valueForKey:@"url"];
                     
                 }
                 
                 
            if ([dicImage valueForKey:@"url"] != nil) {
             
               
//                NSString *finalimageUrlStr = imageUrl;
//                if ([imageUrl containsString:@"width"]) {
//
//                    NSArray * imageSeprateArr  = [imageUrl componentsSeparatedByString:@"?"];
//                    NSString *lastString = @"width=300&height=170&mode=crop";
//
//                    finalimageUrlStr = [NSString stringWithFormat:@"%@%@",[imageSeprateArr firstObject],lastString];
//                }
                
                
             [self downloadImageWithURL:[NSURL URLWithString: imageUrl] completionBlock:^(BOOL succeeded, UIImage *image) {
                 if (succeeded) {
                     // change the image in the cell
                    /// cell.imageView.image = image;
                     
                     // cache the image for use later (when scrolling up)
                     imageviewFor.contentMode = UIViewContentModeScaleToFill;
                    imageviewFor.image = image;
                 }
             }];
        }
                 
    }
             
             [imageConatainer addSubview:imageviewFor];

       UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(1, 180, 280, 20)];
             namelabel.font = [UIFont boldSystemFontOfSize:16];
             namelabel.textAlignment = NSTextAlignmentLeft;
           namelabel.text = [[entryArray objectAtIndex:i]valueForKey:@"title"];
        [imageConatainer addSubview:namelabel];
             
        UILabel *descLbl = [[UILabel alloc]initWithFrame:CGRectMake(1, 200, 280, 45)];
             descLbl.font = [UIFont systemFontOfSize:15];
             descLbl.numberOfLines = 2;
            descLbl.text = [[entryArray objectAtIndex:i]valueForKey:@"description"];
            descLbl.textAlignment = NSTextAlignmentNatural;
            [imageConatainer addSubview:descLbl];
         
             UILabel *linelbl = [[UILabel alloc]initWithFrame:CGRectMake(3, 250, 280, 1)];
             linelbl.font = [UIFont systemFontOfSize:15];
             linelbl.backgroundColor = [UIColor lightGrayColor];
            
             [imageConatainer addSubview:linelbl];
             
             
             
             // read more button
             
             UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
             button.tag = i;
             button.titleLabel.font = [UIFont systemFontOfSize:16];
             [button setTitleColor:[UIColor colorWithRed:0/255.0 green:075/255.0 blue:141/255.0 alpha:1.0] forState:UIControlStateNormal];
             [button addTarget:self
                        action:@selector(readMoreBtnClick:)
              forControlEvents:UIControlEventTouchUpInside];
             [button setTitle:@"Read More" forState:UIControlStateNormal];
             [button setFrame:CGRectMake(1, 260 ,100, 35)];
             
             [imageConatainer addSubview:button];
             
             
         }
         
         scrollViewForImage.contentSize = CGSizeMake(320*entryArray.count+30, imageBackgroundView.frame.size.height);
     }
    
    scrollViewForSepcificData.contentSize = CGSizeMake(screenWidth, finalHeight + 175);

}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    if (url!= nil ){
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
  }
}

@end
