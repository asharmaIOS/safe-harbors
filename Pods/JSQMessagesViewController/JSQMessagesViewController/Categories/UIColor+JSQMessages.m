//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "UIColor+JSQMessages.h"

@implementation UIColor (JSQMessages)

#pragma mark - Message bubble colors

+ (UIColor *)jsq_messageBubbleGreenColor
{
    return [UIColor colorWithRed:148.0f/255.0f
                           green:97.0f/255.0f
                            blue:128.0f/255.0f
                           alpha:1.0f];
    
}

+ (UIColor *)jsq_messageBubbleBlueColor
{
    return [UIColor colorWithRed:133.0f/255.0f
                           green:171.0f/255.0f
                            blue:221.0f/255.0f
                           alpha:1.0f];
}

+ (UIColor *)jsq_messageBubbleRedColor
{
    // return [UIColor colorWithRed:240.0f/255.0f
    // green:150.0f/255.0f
    // blue:66.0f/255.0f
    // alpha:1.0f];
    return [UIColor colorWithRed:133.0f/255.0f
                           green:171.0f/255.0f
                            blue:221.0f/255.0f
                           alpha:1.0f];
}

+ (UIColor *)jsq_messageBubbleLightGrayColor
{
    // return [UIColor colorWithRed:240.0f/255.0f
    // green:150.0f/255.0f
    // blue:66.0f/255.0f
    // alpha:1.0f];
    return [UIColor colorWithRed:133.0f/255.0f
                           green:171.0f/255.0f
                            blue:221.0f/255.0f
                           alpha:1.0f];
}

#pragma mark - Utilities

- (UIColor *)jsq_colorByDarkeningColorWithValue:(CGFloat)value
{
    NSUInteger totalComponents = CGColorGetNumberOfComponents(self.CGColor);
    BOOL isGreyscale = (totalComponents == 2) ? YES : NO;
    
    CGFloat *oldComponents = (CGFloat *)CGColorGetComponents(self.CGColor);
    CGFloat newComponents[4];
    
    if (isGreyscale) {
        newComponents[0] = oldComponents[0] - value < 0.0f ? 0.0f : oldComponents[0] - value;
        newComponents[1] = oldComponents[0] - value < 0.0f ? 0.0f : oldComponents[0] - value;
        newComponents[2] = oldComponents[0] - value < 0.0f ? 0.0f : oldComponents[0] - value;
        newComponents[3] = oldComponents[1];
    }
    else {
        newComponents[0] = oldComponents[0] - value < 0.0f ? 0.0f : oldComponents[0] - value;
        newComponents[1] = oldComponents[1] - value < 0.0f ? 0.0f : oldComponents[1] - value;
        newComponents[2] = oldComponents[2] - value < 0.0f ? 0.0f : oldComponents[2] - value;
        newComponents[3] = oldComponents[3];
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGColorRef newColor = CGColorCreate(colorSpace, newComponents);
	CGColorSpaceRelease(colorSpace);
    
	UIColor *retColor = [UIColor colorWithCGColor:newColor];
	CGColorRelease(newColor);
    
    return retColor;
}

@end
